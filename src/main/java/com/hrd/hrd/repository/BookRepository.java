package com.hrd.hrd.repository;

import com.hrd.hrd.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(path = "books")
public interface BookRepository extends JpaRepository<Book,Integer> {

    @RestResource(path = "findByTitle",rel = "findByTitleContainsIgnoreCase")
    Page<Book> findByTitleContainsIgnoreCase(String title, Pageable pageable);

    @RestResource(path = "findByCategoryTitle",rel = "findByTitleContainsIgnoreCase")
    Page<Book> findByCategoryTitle(String categoryTitle, Pageable pageable);
}
