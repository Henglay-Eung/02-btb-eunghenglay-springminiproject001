package com.hrd.hrd.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileUploadService {
    private final Path root = Paths.get("src/main/resources/static/image/");

    @Value("${file.upload.server.path}")
    private String serverPath;

    public void init(){
        try{
            Files.createDirectory(root);
        }catch (IOException ex){
            throw new RuntimeException("Could not initialize folder for upload");
        }
    }

    public String save(MultipartFile file) {

        String fileName = "";
        String fileDownloadUri = "";

        if(!file.isEmpty()) {
            fileName = file.getOriginalFilename();
            fileDownloadUri = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            try {
                Files.copy(file.getInputStream(), Paths.get(serverPath, fileDownloadUri));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileDownloadUri;

    }

    public Resource load(String filename) {

        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
