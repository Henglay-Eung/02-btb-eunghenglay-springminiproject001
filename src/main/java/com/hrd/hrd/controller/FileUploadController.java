package com.hrd.hrd.controller;

import com.hrd.hrd.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FileUploadController {

    @Autowired
    FileUploadService fileUploadService;

    @Value(value = "${file.upload.server.path}")
    private String serverPath;

    @Value("${file.base.url}")
    private String imageUrl;

    @PostMapping("/upload")
    public ResponseEntity<Map<String,Object>> uploadFile(@RequestParam("file") MultipartFile file) {

        Map<String, Object> res = new HashMap<>();

        try {
            String fileName = fileUploadService.save(file);

            res.put("message","File has been saved successfully");
            res.put("status","Ok");
            res.put("file",imageUrl+fileName);

            res.put("time",new Timestamp(System.currentTimeMillis()));

            return ResponseEntity.status(HttpStatus.OK).body(res);

        } catch (Exception e) {

            res.put("message","Could not upload the file:");
            res.put("status",false);
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(res);
        }
    }
}
